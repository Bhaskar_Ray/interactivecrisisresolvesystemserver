<?php




// include db connect class
require_once __DIR__ . '/reportDbConnect.php';

// connecting to db
$db = new DB_CONNECT();

if (isset($_GET["incident_id"])) {

    $incident_id = $_GET['incident_id'];
    $incident_id = intval($incident_id);


    $result = mysql_query("SELECT * FROM `comment` WHERE incident_id = $incident_id;");

    if (mysql_num_rows($result) > 0) {
        $response["incident_comment"] = array();

        while ($row = mysql_fetch_array($result)) {
            $incident_comment = array();


            $incident_comment["comment"] = $row["comment"];
            $incident_comment["comment_date"] = $row["comment_date"];
            $tempUserid = $row["user_id"];
            $resUser = mysql_query("SELECT user_name FROM  `user_tbl` WHERE user_id=$tempUserid;") or die(mysql_error());
            $rwUser = mysql_fetch_array($resUser);
            $incident_comment['user_name'] = $rwUser['user_name'];

            array_push($response["incident_comment"], $incident_comment);
        }


        // success
        $response["success"] = 1;

        // echoing JSON response
        echo json_encode($response);

    } else {
        // no products found
        $response["success"] = 0;
        $response["message"] = "No incident report found";

        // echo no users JSON
        echo json_encode($response);
    }
} else {
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";
    echo json_encode($response);
}

?>
