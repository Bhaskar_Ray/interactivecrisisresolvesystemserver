<?php
try {
    $db = new PDO('mysql:host=localhost;dbname=codencra_crisis_resolve;charset=utf8', 'codencra_brs', 'iE*ZIx(w_~{u');
    //echo "Connected";
} catch (PDOException $e) {
    //print "Error!: " . $e->getMessage() . "<br/>";
    die();
}

$un_id = $_GET['unt_id'];

$query = $db->prepare("SELECT shelter_name,lat,lon,shelter_condition,no_floor,floor_space,capacity,no_toilet,water,structure_type,type_use FROM shelter where un_id=$un_id");
$query->execute();

if ($query->rowCount() > 0) {
    $data = $query->fetchAll(PDO::FETCH_ASSOC);

    $json['success'] = 1;
    $json['message'] = 'Data found';
    $json['shelter'] = $data;

    echo json_encode($json);
} else {
    $json['success'] = 0;
    $json['message'] = 'No data found';
    $json['shelter'] = '';

    echo json_encode($json);
}
?>