<?php

// array for JSON response
$response = array();



// include db connect class
require_once __DIR__ . '/reportDbConnect.php';

// connecting to db
$db = new DB_CONNECT();
if (isset($_GET["locality"])) {
    $locality = $_GET['locality'];
}

if ($locality == "") {
    $result = mysql_query("SELECT * FROM  `incident`;") or die(mysql_error());

} else {
    $result = mysql_query("SELECT * FROM  `incident` WHERE locality = '$locality';") or die(mysql_error());
}


if (mysql_num_rows($result) > 0) {
    $response["incident"] = array();

    while ($row = mysql_fetch_array($result)) {
        $incident = array();

        $incident["incident_id"] = $row["incident_id"];
        $incident["incident_title"] = $row["incident_title"];
        $incident["incident_lat"] = $row["incident_lat"];
        $incident["incident_lon"] = $row["incident_lon"];
        $incident["locality"] = $row["locality"];
        $temp = $row["type_id"];

        $res = mysql_query("SELECT type_icon FROM  `type_tbl` WHERE type_id=$temp;") or die(mysql_error());
        $rw = mysql_fetch_array($res);
        $incident['icon'] = $rw['type_icon'];
        array_push($response["incident"], $incident);
    }


    // success
    $response["success"] = 1;

    // echoing JSON response
    echo json_encode($response);

} else {
    // no products found
    $response["success"] = 0;
    $response["message"] = "No incident report found";

    // echo no users JSON
    echo json_encode($response);
}

?>
