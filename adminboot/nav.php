<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Interactive Crisis Resolve System Admin Panel</a>
    </div>
    <!-- Top Menu Items -->
    <ul class="nav navbar-right top-nav">


        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>  <?php echo $_SESSION['admin_name'] ?> <b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li>
                    <a data-toggle='modal' data-target='#admin'><i class="fa fa-fw fa-user"></i> Profile</a>
                </li>

                <li class="divider"></li>
                <li>
                    <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                </li>
            </ul>
        </li>
    </ul>
    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav side-nav">
            <li class="active">
                <a href="main.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
            </li>


            <li>
                <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-edit"></i> Cycolne Center <i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="demo" class="collapse">
                    <li>
                        <a href="add_district.php">Add District</a>
                    </li>

                    <li>
                        <a href="district_show.php">Update District</a>
                    </li>

                    <li>
                        <a href="add_upazila.php">Add Upazila</a>
                    </li>
                    <li>
                        <a href="update_upazila.php">Update Upazila</a>
                    </li>
                    <li>
                        <a href="add_union.php">Add Union</a>
                    </li>
                    <li>
                        <a href="update_union.php">Update Union</a>
                    </li>
                    <li>
                        <a href="add_shelter.php">Add Shelter</a>
                    </li>
                    <li>
                        <a href="update_shelter.php">Update Shelter</a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="javascript:;" data-toggle="collapse" data-target="#demo4"><i class="fa fa-fw fa-edit"></i> Incident Report <i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="demo4" class="collapse">
                    <li>
                        <a href="view_delete_report.php">Incident Report</a>
                    </li>

                </ul>
            </li>
            <li>
                <a href="javascript:;" data-toggle="collapse" data-target="#demo1"><i class="fa fa-fw fa-edit"></i> Alert SMS <i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="demo1" class="collapse">
                    <li>
                        <a href="alert_sms_ui.php">Alert SMS</a>
                    </li>

                </ul>
            </li>

            <li>
                <a href="javascript:;" data-toggle="collapse" data-target="#demo_user"><i class="fa fa-fw fa-edit"></i>User <i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="demo_user" class="collapse">
                    <li>
                        <a href="user_show.php">View User</a>
                    </li>

                </ul>
            </li>






        </ul>
    </div>
    <!-- /.navbar-collapse -->




</nav>
