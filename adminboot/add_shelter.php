<?php
//Start session
session_start();

//Check whether the session variable SESS_MEMBER_ID is present or not
if(!isset($_SESSION['admin_id']) || (trim($_SESSION['admin_id']) == ''))
{
    // echo "hello";
    // echo "session userid is = ". $_SESSION['userid'];
    header("location: index.php");
    exit();
}
else
{
    //echo "sessionid prevails";


}

?>








<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Interactive Crisis Resolve System Admin Panel</title>


    <script src="js/jquery-1.11.3.min.js"></script>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">


    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


    <script>


        $(document).ready(function () {
            $.getJSON("get_json_district.php", function (data) {
                $.each(data, function (key, value) {
                    $("#dropDownDest").append($('<option></option>').val(value.dis_id).html(value.dis_name));
                });

            });




        });




        $('#dropDownDest').change(function () {
            // $(#dropDownDest2).html("");
            //alert($(this).val());
            var value=$(this).val();
//Code to select image based on selected car id

            $("#dropDownDest2").html("");
            $("#dropDownDest3").html("");

            $("#dropDownDest2").append($('<option></option>').val("SELECT").html("SELECT"));

            $.getJSON("get_json_upazila.php?dis_id="+value, function (data) {
                $.each(data, function (key, value) {
                    $("#dropDownDest2").append($('<option></option>').val(value.up_id).html(value.up_name));
                });

            });
        });




        $('#dropDownDest2').change(function () {
            // $(#dropDownDest2).html("");
            //alert($(this).val());
            var value=$(this).val();
//Code to select image based on selected car id

            $("#dropDownDest3").html("");
            $("#dropDownDest3").append($('<option></option>').val("SELECT").html("SELECT"));

            $.getJSON("get_json_union.php?up_id="+value, function (data) {
                $.each(data, function (key, value) {
                    $("#dropDownDest3").append($('<option></option>').val(value.un_id).html(value.un_name));
                });

            });
        });












    </script>



</head>

<body>

<div id="wrapper">

    <!-- Navigation -->

    <?php

    include 'nav.php';


    ?>












    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        Add Shelter
                    </h1>
                    <ol class="breadcrumb">
                        <li class="active">
                            <i class="fa fa-dashboard"></i>Add Shelter Panel
                        </li>
                    </ol>
                </div>
            </div>
            <!-- /.row -->




            <div class="container">
                <div class="row">

                    <div class="col-lg-2">



                    </div>
                    <div class="col-lg-8">
                        <div class="panel panel-default">
                            <div class="panel-body table-responsive">

















                                <form name="add_union_form" accept-charset="UTF-8" action="add_shelter_perform.php" method="POST" onsubmit="return ValidInfo()">



                                    <select name="dis" id="dropDownDest" style="margin-bottom:10px;" class="form-control">

                                        <option value="SELECT" selected="selected">SELECT</option>
                                    </select>


                                    <select name="up" id="dropDownDest2" style="margin-bottom:10px;"  class="form-control" required>


                                        <option value="SELECT" selected="selected">SELECT</option>
                                    </select>



                                    <select name="un" id="dropDownDest3" style="margin-bottom:10px;"  class="form-control" required>


                                        <option value="SELECT" selected="selected">SELECT</option>
                                    </select>



                                    <input type="text" class="form-control" name="shelter_name" value="" placeholder="Shelter Name" required  style="margin-bottom:10px;">
                                    <input type="text" class="form-control" name="shelter_condition" value="" placeholder="Shelter Condition" required  style="margin-bottom:10px;">
                                    <input type="text" class="form-control" name="no_floor" value="" placeholder="No of Floor" required  style="margin-bottom:10px;">
                                    <input type="text" class="form-control" name="floor_space" value="" placeholder="Floor Space" required  style="margin-bottom:10px;">
                                    <input type="text" class="form-control" name="structure_type" value="" placeholder="Structure Type" required  style="margin-bottom:10px;">
                                    <input type="text" class="form-control" name="capacity" value="" placeholder="Shelter Capacity" required  style="margin-bottom:10px;">
                                    <input type="text" class="form-control" name="no_toilet" value="" placeholder="No of Toilet" required  style="margin-bottom:10px;">
                                    <input type="text" class="form-control" name="water" value="" placeholder="Availability of Water" required  style="margin-bottom:10px;">
                                    <input type="text" class="form-control" name="type_use" value="" placeholder="Type of Use" required  style="margin-bottom:10px;">
                                    <input type="text" class="form-control" name="lat" value="" placeholder="Shelter Latitude Value" required  style="margin-bottom:10px;">
                                    <input type="text" class="form-control" name="lon" value="" placeholder="Shelter Longitude Value" required  style="margin-bottom:10px;">



                                    <button class="btn btn-info center-block"  type="submit">Add</button>


                                </form>











                            </div>
                        </div>

                    </div>
                    <div class="col-lg-2">

                    </div>

                </div>
            </div>



            <div class="modal fade" id="admin" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                            <h4 class="modal-title custom_align" id="Heading">Admin Information</h4>
                        </div>
                        <div class="modal-body">

                            <div class="alert alert-success">

                                <table class="table">

                                    <tr>




                                        <td>
                                            <p>Admin Name: </p>
                                        </td>

                                        <td>
                                            <?php echo $_SESSION['admin_name'];    ?>
                                        </td>





                                    </tr>


                                    <tr>




                                        <td>
                                            <p>Email: </p>
                                        </td>

                                        <td>
                                            <?php echo $_SESSION['admin_email'];    ?>
                                        </td>





                                    </tr>





                                </table>



                            </div>

                        </div>
                        <div class="modal-footer ">




                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>



            <!-- jQuery -->

            <script src="js/jquery-1.11.3.min.js"></script>

            <!--<script src="js/jquery.js"></script>-->

            <!-- Bootstrap Core JavaScript -->
            <script src="js/bootstrap.min.js"></script>





</body>

</html>


<script>







    function  ValidInfo()
    {
        var a=document.forms["add_union_form"]["un"].value;




        if ((a=="SELECT"))
        {

            alert("Select a union first!!");
            return false;









        }









    }
</script>




<script>


    $(document).ready(function () {
        $.getJSON("get_json_district.php", function (data) {
            $.each(data, function (key, value) {
                $("#dropDownDest").append($('<option></option>').val(value.dis_id).html(value.dis_name));
            });

        });




    });




    $('#dropDownDest').change(function () {
        // $(#dropDownDest2).html("");
        //alert($(this).val());
        var value=$(this).val();
//Code to select image based on selected car id

        $("#dropDownDest2").html("");
        $("#dropDownDest3").html("");

        $("#dropDownDest2").append($('<option></option>').val("SELECT").html("SELECT"));
        $("#dropDownDest3").append($('<option></option>').val("SELECT").html("SELECT"));

        $.getJSON("get_json_upazila.php?dis_id="+value, function (data) {
            $.each(data, function (key, value) {
                $("#dropDownDest2").append($('<option></option>').val(value.up_id).html(value.up_name));
            });

        });
    });




    $('#dropDownDest2').change(function () {
        // $(#dropDownDest2).html("");
        //alert($(this).val());
        var value=$(this).val();
//Code to select image based on selected car id

        $("#dropDownDest3").html("");
        $("#dropDownDest3").append($('<option></option>').val("SELECT").html("SELECT"));

        $.getJSON("get_json_union.php?up_id="+value, function (data) {
            $.each(data, function (key, value) {
                $("#dropDownDest3").append($('<option></option>').val(value.un_id).html(value.un_name));
            });

        });
    });












</script>


