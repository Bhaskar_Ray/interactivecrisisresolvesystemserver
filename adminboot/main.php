<?php
//Start session
session_start();

//Check whether the session variable SESS_MEMBER_ID is present or not
if(!isset($_SESSION['admin_id']) || (trim($_SESSION['admin_id']) == ''))
{
    // echo "hello";
    // echo "session userid is = ". $_SESSION['userid'];
    header("location: login.php");
    exit();
}
else
{
    //echo "sessionid prevails";


}

?>




<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Interactive Crisis Resolve System Admin Panel</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->

        <?php

        include 'nav.php';


        ?>
















        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Dashboard
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Dashboard
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->



                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="row" align="center">
                                    <div class="col-xs-12">
                                        <i class="fa fa-home fa-4x"></i>
                                        <p>Cyclone Shelter</p>
                                    </div>

                                </div>
                            </div>
                            <a href="update_shelter.php">
                                <div class="panel-footer">
                                    <span class="pull-left">Update Shelter</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-green">
                            <div class="panel-heading">
                                <div class="row" align="center">
                                    <div class="col-xs-12">
                                        <i class="fa fa-tasks fa-4x"></i>
                                        <p>Incident Report</p>
                                    </div>

                                </div>
                            </div>
                            <a href="view_delete_report.php">
                                <div class="panel-footer">
                                    <span class="pull-left">View Incidents</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-red">
                            <div class="panel-heading">
                                <div class="row" align="center">
                                    <div class="col-xs-12">
                                        <i class="fa fa-exclamation-triangle fa-4x"></i>
										<p>Alert Notification</p>
                                    </div>
                                    
                                </div>
                            </div>
                            <a href="alert_sms_ui.php">
                                <div class="panel-footer">
                                    <span class="pull-left">Send Alert Notification</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-yellow">
                            <div class="panel-heading">
                                <div class="row" align="center">
                                    <div class="col-xs-12">
                                        <i class="fa fa-user fa-4x"></i>
                                        <p>User Info</p>
                                    </div>

                                </div>
                            </div>
                            <a href="user_show.php">
                                <div class="panel-footer">
                                    <span class="pull-left">View Users</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <!-- /.row -->


                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-4">

                    </div>
                    <div class="col-lg-4">

                    </div>
                    <div class="col-lg-4">

                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->


        <div class="modal fade" id="admin" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                        <h4 class="modal-title custom_align" id="Heading">Admin Information</h4>
                    </div>
                    <div class="modal-body">

                        <div class="alert alert-success">

                            <table class="table">

                                <tr>




                                    <td>
                                        <p>Admin Name: </p>
                                    </td>

                                    <td>
                                        <?php echo $_SESSION['admin_name'];    ?>
                                    </td>





                                </tr>


                                <tr>




                                    <td>
                                        <p>Email: </p>
                                    </td>

                                    <td>
                                        <?php echo $_SESSION['admin_email'];    ?>
                                    </td>





                                </tr>





                            </table>



                        </div>

                    </div>
                    <div class="modal-footer ">





                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>


    <!-- jQuery -->
    <script src="js/jquery.js"></script>
     <script src="js/jquery-1.11.3.min.js"></script>


        <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>



</body>

</html>
