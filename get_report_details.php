<?php

// array for JSON response
$response = array();



// include db connect class
require_once __DIR__ . '/reportDbConnect.php';

// connecting to db
$db = new DB_CONNECT();

if (isset($_GET["incident_id"])) {

    $incident_id = $_GET['incident_id'];
    $incident_id = intval($incident_id);


    $result = mysql_query("SELECT * FROM `incident` WHERE incident_id = $incident_id;");

    if (mysql_num_rows($result) > 0) {
        $response["incident_details"] = array();

        while ($row = mysql_fetch_array($result)) {
            $incident_details = array();


            $incident_details["incident_title"] = $row["incident_title"];
            $incident_details["severity_level"] = $row["severity_level"];
            $incident_details["source"] = $row["source"];
            $incident_details["description"] = $row["description"];
            $incident_details["impact"] = $row["impact"];
            $incident_details["incident_date"] = $row["incident_date"];
            $incident_details["incident_time"] = $row["incident_time"];
            $incident_details["submission_date"] = $row["submission_date"];
            $incident_details["locality"] = $row["locality"];
            $incident_details["image_path"] = $row["image_path"];
            $tempUserid = $row["user_id"];
            $resUser = mysql_query("SELECT user_name FROM  `user_tbl` WHERE user_id=$tempUserid;") or die(mysql_error());
            $rwUser = mysql_fetch_array($resUser);
            $incident_details['user_name'] = $rwUser['user_name'];
            $tempTypeid = $row["type_id"];
            $resType = mysql_query("SELECT type_name FROM  `type_tbl` WHERE type_id=$tempTypeid;") or die(mysql_error());
            $rwType = mysql_fetch_array($resType);
            $incident_details['type_name'] = $rwType['type_name'];
            array_push($response["incident_details"], $incident_details);
        }


        // success
        $response["success"] = 1;

        // echoing JSON response
        echo json_encode($response);

    } else {
        // no products found
        $response["success"] = 0;
        $response["message"] = "No incident report found";

        // echo no users JSON
        echo json_encode($response);
    }
} else {
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";
    echo json_encode($response);
}

?>
