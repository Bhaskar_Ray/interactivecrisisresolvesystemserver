<?php


if (isset($_POST['image_n']) && isset($_POST['image'])
    && isset($_POST['latitude']) && isset($_POST['longitude'])
    && isset($_POST['title']) && isset($_POST['reportCategoryId'])
    && isset($_POST['description']) && isset($_POST['severity']) && isset($_POST['reportSource']) && isset($_POST['impact']) && isset($_POST['time'])
    && isset($_POST['date']) && isset($_POST['user_id']) && isset($_POST['locality'])
) {


    $imageName = $_POST['image_n'];
    $base64Data = $_POST['image'];
    $incident_lat = $_POST['latitude'];
    $incident_lon = $_POST['longitude'];
    $incident_title = $_POST['title'];
    $type_id = $_POST['reportCategoryId'];
    $type_id = intval($type_id);
    $description = $_POST['description'];
    $severity_level = $_POST['severity'];
    $source = $_POST['reportSource'];
    $impact = $_POST['impact'];
    $incident_time = $_POST['time'];
    $incident_date = $_POST['date'];
    $user_id = $_POST['user_id'];
    $user_id = intval($user_id);

   // $submission_date = $_POST['submission_date'];
    $dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
    $submission_date=date_format($dt,'Y-m-d H:i:s');
    $locality = $_POST['locality'];

    if (empty($imageName) && empty($base64Data)) {
        $image_path = "NoImage";

    } else {
        //decode
        $binary = base64_decode($base64Data);
        header('Content-Type: bitmap; charset=utf-8');
        //$name="test.jpg";
        //$imageName=  rand(10, 10000).$name;

        $file = fopen('adminboot/images/' . $imageName, 'wb');
        fwrite($file, $binary);
        fclose($file);
        $image_path = $imageName;

    }
    if (empty($locality)) {
        $locality = "Unknown";
    }


    require_once __DIR__ . '/reportDbConnect.php';

    $db = new DB_CONNECT();

    $sql = "Insert into incident (incident_title,severity_level,source,description,impact,incident_date,incident_time,submission_date,locality,incident_lat,incident_lon,image_path,user_id,type_id) values ('" . $incident_title . "','" . $severity_level . "','" . $source . "','" . $description . "','" . $impact . "','" . $incident_date . "','" . $incident_time . "','" . $submission_date . "','" . $locality . "','" . $incident_lat . "','" . $incident_lon . "','" . $image_path . "','" . $user_id . "','" . $type_id . "')";

    $inserted = mysql_query($sql);

    $response = array();
    if ($inserted) {
        $response['success'] = 1;
        $response["message"] = "Information successfully added";
        echo json_encode($response);


    } else {

        $response['success'] = 0;
        $response["message"] = "Required field(s) is missing";
        echo json_encode($response);


    }
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";

    // echoing JSON response
    echo json_encode($response);
}







?>